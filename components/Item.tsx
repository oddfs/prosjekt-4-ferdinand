import React, { ReactElement } from 'react';
import { Image, StyleSheet } from 'react-native';
import { ListItem } from '@ui-kitten/components';
import { IPokemon } from '../types';
import { connect } from 'react-redux';
import { startSetDisplayPokemon } from './../store/actions/displayPokemon';
import styled from 'styled-components';

const styles = StyleSheet.create({
  pokemonicon: {
    width: 70,
    height: 70,
  },
  pagerImage: {
    height: '50%',
    width: '100%',
    resizeMode: 'contain',
  },
});

/**
 * Each item as they show up in the list on the first page.
 * @param pokemon A single IPokemon element
 * @param startSetDisplayPokemon function that fires dispatcher from redux to update state for displayPokemon
 * @param props Prop drilled function from ViewPager. Original function "setViewPage"
 */
const PokemonItem = ({
  pokemon,
  startSetDisplayPokemon,
  props,
}: {
  pokemon: IPokemon;
  startSetDisplayPokemon: (pokemon: IPokemon) => void;
  props: (page: number) => void;
}): ReactElement => {
  const { Name, Type1, Type2, Num } = pokemon;
  return (
    <ListItem
      onPress={() => {
        startSetDisplayPokemon(pokemon);
        props(2);
      }}
      title={Name}
      description={Type1 + ' ' + Type2}
      accessoryLeft={() => ItemImage(Num, true)}
      key={Num}
    />
  );
};

/**
 * Extracts a pokemon image from an api and returns a react-native Image element
 * @param number pokemon number, as they are indexed normally in a pokedex
 * @param onList Refers to where the picture is going to get rendered for style purposes
 */
export const ItemImage = (
  pokemonNumber: number,
  onList: boolean
): ReactElement => {
  let stringNumber = '';
  if (pokemonNumber < 10) {
    stringNumber = '00' + pokemonNumber.toString();
  }
  if (pokemonNumber >= 10 && pokemonNumber < 100) {
    stringNumber = '0' + pokemonNumber.toString();
  }
  if (pokemonNumber >= 100 && pokemonNumber <= 151) {
    stringNumber = pokemonNumber.toString();
  }

  //Custom image link for each pokemon
  let imagelink =
    'https://assets.pokemon.com/assets/cms2/img/pokedex/full/' +
    stringNumber +
    '.png';
  return (
    <Image
      style={onList ? styles.pokemonicon : styles.pagerImage}
      source={{ uri: imagelink }}
    />
  );
};

const styledPokemonItem = styled(PokemonItem)`
  background-color: 'rgba(0, 0, 0, 0.0)';
`;

export default connect(null, { startSetDisplayPokemon })(PokemonItem);
