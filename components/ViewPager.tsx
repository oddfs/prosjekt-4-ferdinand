import React, { ReactElement } from 'react';
import { ImageBackground } from 'react-native';
import { Layout, ViewPager } from '@ui-kitten/components';
import ItemWrapper from './ItemWrapper';
import PokemonPage from './PokemonPage';
import Search from './Search';
import { connect } from 'react-redux';
import { StoreState } from './../types';
import styled from 'styled-components/native';

const Pager = styled(ViewPager)`
  flex: 1;
  align-items: center;
`;

/**
 * Works like a router from React. I set the nubmer
 * of pages that you can navigate between. Here we
 * have the pokemonlist and each element by themselves
 */
export const PagerFunction = (): ReactElement => {
  const [selectedIndex, setSelectedIndex] = React.useState(0);

  return (
    <Pager
      selectedIndex={selectedIndex}
      onSelect={(index) => setSelectedIndex(index)}
    >
      <Layout level="1">
        <ImageBackground
          style={{ width: '100%', height: '100%' }}
          source={{ uri: 'https://cdn.wallpapersafari.com/89/48/Ph6EHu.png' }}
        >
          <Search />

          {/* Propdrills this function down to each item. Not a great solution, but it works!*/}
          <ItemWrapper props={setSelectedIndex} />
        </ImageBackground>
      </Layout>
      <Layout level="2">
        <PokemonPage />
      </Layout>
    </Pager>
  );
};

const mapStateToProps = (state: StoreState) => {
  return {
    displayPokemon: state.displayPokemon,
  };
};

export default connect(mapStateToProps)(PagerFunction);
