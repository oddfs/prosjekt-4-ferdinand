import React from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import { IPokemon, StoreState } from './../types';
import { ItemImage } from './Item';
import styled from 'styled-components';
import { Text } from '@ui-kitten/components';
import { CenteredText } from './Search';

const Content = styled(View)`
  justify-content: center;
  align-self: center;
  height: 100%;
  width: 100%;
`;
const Container = styled(View)`
  flex-direction: row;
  align-self: center;
`;

const TextStyle = styled(Text)`
  color: white;
  text-align: center;
`;

const TextContainer = styled(View)`
  background-color: black;
  border-radius: 30px;
  margin: 3%;
  padding: 2.5%;
`;

const TotalContainer = styled(View)`
  background-color: #cc0000;
  border-radius: 30px;
  margin: 3%;
  padding: 2.5%;
`;
/**
 * This is the ViewPage for each pokemon after they are clicked on the first page.
 * @param displayPokemon The pokemon that has been set by setDisplayPokemon in the PokemonItem class/function.
 */
const PokemonPage = ({ displayPokemon }: { displayPokemon: IPokemon }) => {
  const { Name, Type1, Type2, Num, Total } = displayPokemon;
  return (
    <Content>
      {ItemImage(Num, false)}
      <CenteredText category="h1">{Name}</CenteredText>
      <Container>
        <TextContainer>
          <TextStyle category="h4" status="warning">
            {Type1 + ' '}
          </TextStyle>
        </TextContainer>
        {Type2 ? (
          <TextContainer>
            <TextStyle category="h4">{Type2}</TextStyle>
          </TextContainer>
        ) : null}
      </Container>
      <TotalContainer>
        <TextStyle category="h3">{'Total: ' + Total}</TextStyle>
      </TotalContainer>
    </Content>
  );
};

const mapStateToProps = (state: StoreState) => {
  return {
    displayPokemon: state.displayPokemon,
  };
};

export default connect(mapStateToProps)(PokemonPage);
