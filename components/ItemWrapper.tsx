import React, { ReactElement, useEffect, useState } from 'react';
import {
  startGetPokemon,
  startSearchPokemon,
} from './../store/actions/pokemons';
import { connect } from 'react-redux';
import { IPokemon, StoreState } from './../types';
import paginator from './../utilities/paginator';
import { Text, View } from 'react-native';
import PokemonItem from './Item';
import styled from 'styled-components/native';
import { List } from '@ui-kitten/components';

const StyledView = styled(View)`
  height: 65%;
`;

const ListStyled = styled(List)`
  background-color: 'rgba(0,0,0,0)';
`;

/**
 * Displays all pokemon in the database and reacts to filters from search. Inhabits the paginator function as well.
 * @param pokemons Array of pokemons from the redux state
 * @param startGetPokemon function that fires dispatcher from redux to update state for pokemons
 * @param props Prop drilled function from ViewPager. Original function "setViewPage"
 */
const ItemWrapper = ({
  pokemons,
  startGetPokemon,
  props,
}: {
  pokemons: Array<IPokemon>;
  startGetPokemon: VoidFunction;
  props: (page: number) => void;
}): ReactElement => {
  const [pokemonCount, setPokemonCount] = useState(7);

  // Pagination listener/function. updates whenever setPokemonCount function is fired to increase amount of pokemon shown
  paginator(setPokemonCount, 7);

  // Fetches all when component has mounted so that the pokemons are up to date with the current state.
  useEffect(() => {
    startGetPokemon();
  }, [startGetPokemon, startSearchPokemon]);

  const listPokemon = pokemons.slice(
    0,
    Math.min(pokemonCount, pokemons.length)
  );

  const handleLoad = () => setPokemonCount(pokemonCount + 5);

  return (
    <StyledView>
      {listPokemon.length > 0 ? (
        <ListStyled
          data={listPokemon}
          renderItem={(pokemon) => (
            <PokemonItem
              props={props}
              key={pokemon.item.Num}
              pokemon={pokemon.item}
            />
          )}
          onEndReached={handleLoad}
        />
      ) : (
        <Text>Ingen treff</Text>
      )}
    </StyledView>
  );
};

const mapStateToProps = (state: StoreState) => {
  return {
    pokemons: state.pokemons.pokemons,
  };
};

const mapDispatchToProps = {
  startGetPokemon,
};

export default connect(mapStateToProps, mapDispatchToProps)(ItemWrapper);
