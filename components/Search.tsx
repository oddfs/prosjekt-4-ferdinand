import React, { ReactElement, useState } from 'react';
import { View, Image } from 'react-native';
import { Input, Icon, Text, Toggle } from '@ui-kitten/components';
import styled from 'styled-components/native';
import { connect } from 'react-redux';
import { startSearchPokemon } from './../store/actions/pokemons';
import { IPokemon, Sort, StoreState } from './../types';

const SearchIcon = (props: any) => <Icon name="search-outline" {...props} />;

const Container = styled(View)`
padding-top: 15%
  justify-content: space-around;
  height: 35%;
`;

export const CenteredText = styled(Text)`
  align-self: center;
`;

export const HeaderText = styled(Text)`
  align-self: center;
  font-size: 45px;
`;

const SearchInput = styled(Input)`
  width: 90%;
  align-self: center;
`;

/**
 * Takes values from the searchfield and fires the action dispatcher for the redux searchfunction 
 * @param startSearchPokemon fires de action dispatcher with the attributes given below. 

 */
const Search = ({
  startSearchPokemon,
}: {
  startSearchPokemon: (
    search: string,
    sortingElement: Sort.ALFABETICAL | Sort.NUMERICAL
  ) => void;
}): ReactElement => {
  // Setting the default sort state and updates whenever the user wants to change the filter
  const [activeSort, setActiveSort] = useState<
    Sort.ALFABETICAL | Sort.NUMERICAL
  >(Sort.ALFABETICAL);
  const [searchWord, setSearchWord] = useState<string>('');

  /**
   * fires the action dispatcher to search for pokemon
   * @param search the text from the inputfield
   * @param sortingElement Alfabetical/Numerical sorting
   */
  const dispatchSearchAndSort = (
    search: string,
    sortingElement: Sort.NUMERICAL | Sort.ALFABETICAL
  ) => {
    startSearchPokemon(search, sortingElement);
  };

  /**
   * Updates sorting criterias and dispatches new search and sort
   * @param sortingElement The attribute to sort on
   */
  const handleSort = (sortingElement: Sort.ALFABETICAL | Sort.NUMERICAL) => {
    dispatchSearchAndSort(searchWord, sortingElement);
  };

  /**
   * Updates the toggle when clicked.
   * @param isChecked returns a boolean
   */
  const [checked, setChecked] = React.useState<boolean>(false);
  const onCheckedChange = (isChecked: boolean) => {
    if (isChecked === true) {
      setActiveSort(Sort.NUMERICAL);

      handleSort(activeSort);
    }
    if (isChecked === false) {
      setActiveSort(Sort.ALFABETICAL);
      handleSort(activeSort);
    }

    setChecked(isChecked);
  };

  return (
    <Container>
      <HeaderText category="h1">Pokedex app</HeaderText>
      <CenteredText category="p1">Sorter alfabetisk?</CenteredText>
      <Toggle checked={checked} onChange={onCheckedChange} />
      <SearchInput
        onChangeText={(text) => {
          dispatchSearchAndSort(text, activeSort);
          setSearchWord(text);
        }}
        placeholder={'Skriv inn navn eller type'}
        accessoryLeft={SearchIcon}
      />
    </Container>
  );
};

const mapDispatchToProps = { startSearchPokemon };

export default connect(null, mapDispatchToProps)(Search);
