/* export interface IPokemon {
  id: number;
  name: string;
  type: string[];
  base: {
    hp: number;
    attack: number;
    defense: number;
    spattack: number;
    spdefense: number;
    speed: number;
  };
} */

export interface IPokemon {
  Name: string;
  Total: number;
  Num: number;
  Type1: string;
  Type2: string;
}

export interface PokemonState {
  pokemons: IPokemon[];
  type?: string;
}

export enum Sort {
  DESC = -1,
  ASC = 1,
  NUMERICAL = 'Num',
  ALFABETICAL = 'Name',
}

export interface StoreState {
  pokemons: PokemonState;
  displayPokemon: IPokemon;
}
