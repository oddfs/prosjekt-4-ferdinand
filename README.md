_Prosjekt 4:_

# Pokedex-App

**Laget av:**

<img src="/assets/trener2.png" height="50px" alt="Trener"/>

Ferdinand Steen-Johnsen

[[_TOC_]]

## Generelt om prosjektet

Dette er en Pokedex som er tilgjengelig på mobile enheter for alle pokemonelskere. Her er det mulig å kunne søke etter dine favoritt pokemon for å få vite mer om dem. I tillegg kan du sortere pokemonene alfabetisk eller numerisk og søke etter typen pokemon i søkefeltet. (f.eks. "Grass", "Fire" osv..).
Håper du liker prosjektet!

## Installering og oppsett av prosjektet

1. Klon repositoriet ned til ønsket lokasjon på maskinen din med:

   ```
   git clone https://gitlab.stud.idi.ntnu.no/oddfs/prosjekt-4-ferdinand.git
   ```

2. Deretter naviger deg inn i mappen "prosjekt-4-ferdinand"

   ```
   cd prosjekt-4-ferdinand
   ```

3. Last ned pakkene fra npm 

   ```
   npm i
   ```

   (Tror jeg har klart å legge til alle tillegg i dependencies, hvis ikke må du laste ned det som mangler :^) )
   

4. Kjør expo klienten med kommandoen

   ```
   npm start
   ```

5. Velg emulator/simulator som du ønsker å se prosjektet på (Det er enklest å bruke mobilen).
   

## Innhold og funksjonalitet

Jeg har valgt alternativ A) for denne oppgaven og har jobbet alene.

### Søkegrensesnitt med resultatsett

Hovedfokuset med appen min er søkefunksjonen. Selve søkefeltet er hentet ut fra tredjepartsbiblioteket UI Kitten. Brukeren klikker seg inn på søkefeltet og tastaturet popper opp for at man kan skrive inn. Deretter kommer et knippe resultater under i en liste.

### Dynamisk pagination

Resultatsettet viser først så mange elementer som fyller skjermen under søkefelt-delen av appen. Deretter laster den inn fler og fler elementer når brukeren scroller seg nedover. Ettersom at jeg har brukt et relativt lite resultatsett, 151 pokemon, så hentes alle 151 elementene først i backend og så bestemmer jeg antallet av hvor mange som vises i frontend. Dette er også fordi bildene til hver pokemon hentes med egne kall til et eksternt API: https://assets.pokemon.com/assets/cms2/img/pokedex/full/" _Pokemonnummer her_ ".png. Her ønsker jeg ikke å laste inn fler bilder enn man må ettersom at dette er en applikasjon som skal brukes på en mobil og folk må betale for egen mobildata dersom de ikke er på Wifi. 

### Detaljer om hvert objekt

Ettersom at jeg valgte å gjenbruke samme backend som fra prosjekt 3 hadde jeg et databasesett med relativt lite informasjon om hvert pokemon element. Derfor er det ikke noen "ekstra detaljer" ennet enn en totalscore på ViewPagen når man klikker på en pokemon. Når det har sagt så har jeg alikevell vist at jeg har klart å mappe et element fra listen og presentere det samme elementet alene på et nytt sted. Dersom det var mer informasjon i databasen å hente så hadde jeg vist frem dette i tillegg. 

### Raffinering av søkeresultat

Pokemon kan sorteres inn etter typer. I den første generasjonen er det 15 forskjellige typer å sortere på. Ettersom at det var så mange forskjellige typer syntes jeg det var mer hensiktsmessig å søke etter typer i søkefeltet enn å ha 15 knapper eller elementer i en nedtrekksliste på en liten skjerm. Dermed kan du søke etter både navn og type for å raffinere søkeresulatet.

I tillegg til dette så kan du bestemme om du ønsker å få fremvist resultatsettet numerisk eller alfabetisk dersom du ønsker det.

## Teknologi, koding, testing, dokumentasjon og levering

### Typescript, React native og Expo

Jeg har brukt typescript til alle delene av appen som jeg har programmert selv. Jeg instansierte appen med:

```
expo init -t expo-template-blank-typescript
```

Videre har jeg brukt expo for testing av applikasjonen. Simulatorenen/emulatoren(?) jeg brukte var en IPhone 12 pro max fra xcode og min egen IPhone 8. Ettersom at jeg har jobbet alene har jeg dessverre hatt lite mulighet til å teste på en Android enhet.

### End-2-end testing

Jeg har foretatt end-2-end tester for iPhone 8, IPhone 11 og IPhone 12 pro max. Testingen gikk ut på kjernefunksjonalitet som søkefunksjonen, detaljer om hvert objekt og at informasjonen ble hentet ut riktig fra databasen. Jeg testet også skjermlengde og at alle elementenene havnet innen for området brukeren kunne se på skjermen. Jeg hadde dessverre ikke tilgang til en androidenhet og fikk dermed ikke testet for disse. Det kan dermed hende at det finnes feil på Android som jeg ikke har oppdaget. 

<img src="/assets/e2egif.gif" height="400px" alt="Trener"/>

## Erfaringer og refleksjon

### Redux og server

Da jeg begynte dette prosjektet ønsket jeg å tilegne meg kunnskap som jeg gikk glipp av i P3. I forrige prosjekt var jeg hovedsakelig ansvarlig for frontend og hadde lite tid til å forstå meg på backend og serveroppsettet. Jeg var heller ikke fornøyd med måten jeg hadde fått brukt redux på og dermed valgte jeg å slå to fluer i en smekk ved å lage nye backend tilkoblinger og å bruke dem direkte i redux. I tillegg satte jeg opp backenden selv på en virtuell maskin slik at informasjonen kan nås fra hvor som helst. Jeg gjenbrukte mye av logikken fra både mitt eget P3 prosjekt og [Gruppe 62](https://gitlab.stud.idi.ntnu.no/it2810-h20/team-62) når det kom til både redux og logiske funksjoner.

Inspirasjonen min for dette var [Gruppe 62](https://gitlab.stud.idi.ntnu.no/it2810-h20/team-62) sitt P3 prosjekt der de brukte redux for så og si alle funksjoner ved applikasjonen. Jeg spurte gruppen om tillatelse til å gjenbruke noe av koden deres ettersom at dette ikke skulle være et vurderingskriterie for dette prosjektet. Dermed prioritere jeg å bruke mye av tiden jeg hadde til disposisjon for å sammenkoble mitt eget oppsett fra p3 med oppsettet til gruppe 62. Dette gjorde at jeg fikk litt mindre tid til å utvikle appen i seg selv, men har lært enda mer om redux som kan brukes på begge platformene. Dermed føler jeg at jeg har tilegnet meg både grunnleggende kunnskap om React native og videregående kunnskap om React.

### Tredjepartsbibliotek

Jeg ønsket å finne et godt bibliotek som jeg kunne bruke til senere prosjekter også. Jeg valgte [UI Kitten](https://akveo.github.io/react-native-ui-kitten/) fordi det er basert på designsystemet fra [Eva Design](https://eva.design/). Det gir en enkel og sømløs overgang mellom å lage design i Figma og å oversette dette videre til enten React eller React native applikasjoner. Ved å bruke et oppsett som dette sparte jeg mye tid på valg av farger og komponenter som skulle inn i appen min. 

### Annet

Den virtuelle maskinen som gruppen min fikk tildelt hadde ikke blitt oppdatert skikkelig på utrolig lenge. Dette medførte mange unødvendige problemstillinger som kunne ha blitt unngått dersom maskinene som ble tildelt hadde vært klare på forhånd. Nevner dette slik at det kan unngås for andre grupper til neste år. 



Bruken av Gitlab  som issue-tracker var fortsatt veldig nyttig selvom man jobbet alene. Liker funksjonene til Gitlab godt!

## Kilder

- [Redux](https://www.freecodecamp.org/news/how-to-use-redux-in-your-react-typescript-app/)
- [Redux forklaring](https://dev.to/oahehc/redux-data-flow-and-react-component-life-cycle-11n)
- [UI Kitten](https://akveo.github.io/react-native-ui-kitten/)
- [Gruppe 06 - Prosjekt 3 (Min gruppe)](https://gitlab.stud.idi.ntnu.no/it2810-h20/team-06/prosjekt-3)
- [Gruppe 62 - Prosjekt 3](https://gitlab.stud.idi.ntnu.no/it2810-h20/team-62/prosjekt-3)
- [Eva Design](https://eva.design/)
- [Styled components](https://styled-components.com/)
- [Styled components tutorial](https://www.youtube.com/watch?v=OqpYDyY2kik&t=328s)

