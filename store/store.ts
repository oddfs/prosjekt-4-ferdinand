import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers';

// Thunk used to dispatch async actions
const middleware = [thunk];

// Create stor with middleware
const store = createStore(rootReducer, applyMiddleware(...middleware));

export default store;
