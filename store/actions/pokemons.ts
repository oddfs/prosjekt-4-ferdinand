import { LOAD, SEARCH, LOAD_ERROR } from './actionTypes';
import { IPokemon, Sort } from '../../types';
import api from '../../utilities/api';
import { Dispatch } from 'react';
import { AnyAction } from 'redux';

// _________ACTION CREATORS_________
// LOAD_Error
export const loadError = (): AnyAction => ({
  type: LOAD_ERROR,
  payload: [false],
});

// GET_POKEMON
export const getPokemon = (pokemons: IPokemon[]): AnyAction => ({
  type: LOAD,
  payload: pokemons,
});

// SEARCH_POKEMON
export const searchPokemon = (pokemons: IPokemon[]): AnyAction => ({
  type: SEARCH,
  payload: pokemons,
});

// _________ACTION DISPATCHERS_________

/**
 * Fetches all pokemons from the database
 */
export const startGetPokemon = () => async (
  dispatch: Dispatch<AnyAction>
): Promise<void> => {
  api
    .get('/')
    .then((res) => {
      dispatch(getPokemon(res.data));
    })
    .catch(() => {
      dispatch(loadError());
    });
};
startGetPokemon();

/**
 * Searches for a sorted array of pokemons.
 * @param search String were searching for
 * @param sortingElement element to sort on
 */

export const startSearchPokemon = (
  search: string,
  sortingElement: Sort.ALFABETICAL | Sort.NUMERICAL
) => async (dispatch: Dispatch<AnyAction>) => {
  if (search === '' && sortingElement === Sort.ALFABETICAL) {
    api
      .get('/Name')
      .then((res) => {
        dispatch(getPokemon(res.data));
      })
      .catch(() => {
        dispatch(loadError());
      });
  } else if (search === '' && sortingElement === Sort.NUMERICAL) {
    api
      .get('/')
      .then((res) => {
        dispatch(getPokemon(res.data));
      })
      .catch(() => {
        dispatch(loadError());
      });
  } else {
    api
      .get('search/' + search + '/' + sortingElement)
      .then((res) => {
        dispatch(searchPokemon(res.data));
      })

      .catch(() => {
        dispatch(loadError());
      });
  }
};
