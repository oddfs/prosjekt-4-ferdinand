import { SET_DISPLAY } from './actionTypes';
import { Dispatch } from 'react';
import { IPokemon } from '../../types';
import { AnyAction } from 'redux';

// ______ACTION CREATORS______
export const setDisplayPokemon = (pokemon: IPokemon): AnyAction => ({
  type: SET_DISPLAY,
  pokemon,
});

// _________ACTION DISPATCHERS_________

/**
 * Dispatches the action to setDisplayPokemon which sends a function call to the reducer setting the display pokemon
 * @param pokemon Pokemon to be displayed in modal
 */
export const startSetDisplayPokemon = (pokemon: IPokemon) => (
  dispatch: Dispatch<AnyAction>
): void => {
  dispatch(setDisplayPokemon(pokemon));
};
