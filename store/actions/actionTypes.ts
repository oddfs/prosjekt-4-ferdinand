// pokemons.ts actions
export const LOAD = 'LOAD';
export const SEARCH = 'SEARCH';
export const LOAD_ERROR = 'LOAD_ERROR';

// displayPokemon.ts actions
export const SET_DISPLAY = 'SET_DISPLAY';
