import { combineReducers } from 'redux';
import pokemons from './pokemonReducer';
import displayPokemon from './displayPokemonReducer';

export default combineReducers({ pokemons, displayPokemon });
