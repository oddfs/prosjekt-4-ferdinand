import { LOAD, SEARCH, LOAD_ERROR } from '../actions/actionTypes';
import { IPokemon, PokemonState } from '../../types';

export default (
  state = { pokemons: [] },
  action: { type: string; payload: IPokemon[] }
): PokemonState => {
  const { type, payload } = action;

  switch (type) {
    case LOAD:
    case SEARCH:
      return { pokemons: payload, type };
    case LOAD_ERROR:
      {
        console.log(
          '____________ERROR___________ \n' +
            'Pokemons in state: \n \n' +
            state.pokemons.map((item: IPokemon) => item.Name + ' ')
        );
      }
      return { pokemons: state.pokemons, type };
    default:
      return state;
  }
};
