import { SET_DISPLAY } from '../actions/actionTypes';
import { IPokemon } from '../../types';

/* Setting an initial store to ensure that we dont get typescript problems */
const initialStore = {
  Name: 'No pokemon selected',
  Total: 0,
  Num: 0,
  Type1: '',
  Type2: '',
};

export default (
  state = initialStore,
  action: { type: string; pokemon: IPokemon | null }
): IPokemon | null => {
  const { type, pokemon } = action;
  switch (type) {
    case SET_DISPLAY:
      return pokemon;
    default:
      return state;
  }
};
