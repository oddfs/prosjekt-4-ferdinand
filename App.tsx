import React from 'react';
import { View } from 'react-native';
import { connect, Provider } from 'react-redux';
import * as eva from '@eva-design/eva';
import {
  ApplicationProvider,
  IconRegistry,
  Layout,
} from '@ui-kitten/components';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import store from './store/store';
import Pager from './components/ViewPager';

/**
 * Initiating the app with the provider around it aswell as the Eva design system and Icon packages
 */
const App = () => {
  return (
    <Provider store={store}>
      <React.Fragment>
        <ApplicationProvider {...eva} theme={eva.light}>
          <IconRegistry icons={EvaIconsPack} />
          <Pager />
        </ApplicationProvider>
      </React.Fragment>
    </Provider>
  );
};

const AppContainer = connect(null)(App);

// Render the app container component with the provider around it
export default class AppWrapper extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <AppContainer />
      </Provider>
    );
  }
}
