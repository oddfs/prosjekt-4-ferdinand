/**
 * Calling a this when the user reaches the bottom of the page to increase amount of pokemon in the list.
 * @param setPokemonCount setState-method for movieCount-state
 * @param step Incrementation step, number of movies to load when we scroll down
 *
 * Source: Group 62
 */
const paginator = (
  setPokemonCount: React.Dispatch<React.SetStateAction<number>>,
  step: number
): void => {
  //eslint-disable-next-line
  window.onscroll = (e: Event) => {
    if (
      window.innerHeight + window.scrollY >=
      document.body.scrollHeight - 200
    ) {
      setPokemonCount((m: number) => m + step);
    }
  };
};

export default paginator;
